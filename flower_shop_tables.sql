CREATE TABLE customers (
    customer_id NUMBER PRIMARY KEY,
    first_name  VARCHAR2(50) NOT NULL,
    last_name   VARCHAR2(50) NOT NULL,
    address     VARCHAR(100),
    phone       VARCHAR2(50)
);


CREATE TABLE branches (
    branch_id NUMBER PRIMARY KEY,
    location_ VARCHAR2(100),
    phone     VARCHAR2(50)
);


CREATE TABLE orders (
    order_id    NUMBER PRIMARY KEY,
    customer_id NUMBER,
    branch_id   NUMBER NOT NULL,
    order_date  DATE NOT NULL,
    
    CONSTRAINT fk_orders_customer_id FOREIGN KEY ( customer_id )
        REFERENCES customers ( customer_id ),
    CONSTRAINT fk_orders_branch_id FOREIGN KEY ( branch_id )
        REFERENCES branches ( branch_id )
);


CREATE TABLE suppliers (
    supplier_id NUMBER,
    name_       VARCHAR(50),
    address     VARCHAR(50),
    CONSTRAINT suppliers_pk PRIMARY KEY(supplier_id)
);


CREATE TABLE flowers (
    flower_id   NUMBER PRIMARY KEY,
    name_       VARCHAR2(50) NOT NULL,
    price       NUMBER NOT NULL CHECK ( price > 0 ),
    supplier_id NUMBER,
    CONSTRAINT flowers_fk FOREIGN KEY ( supplier_id )
        REFERENCES suppliers ( supplier_id )
);


CREATE TABLE order_details (
    order_id  NUMBER NOT NULL,
    flower_id NUMBER NOT NULL,
    quantity  NUMBER NOT NULL CHECK ( quantity > 0 ),
    CONSTRAINT order_details_order_id_fk FOREIGN KEY ( order_id )
        REFERENCES orders ( order_id ),
    CONSTRAINT order_details_flower_id_fk FOREIGN KEY ( flower_id )
        REFERENCES flowers ( flower_id )
);
 